package com.zuitt.example;

public class Variables {
    // for the meantime we will run within the class file
    public static void main(String[] args){
        //Naming conventions
        //the terminology used for variable names is "identifier"
        // Syntax: dataType identifier;

        // variable
        int age;
        char middleName;

        int x;
        int y = 1;

        //initialization after declaration
        x=1;

        //change values
        y=2;

        System.out.println("The value of y is " + y + " and the value of x is " + x);

        //Primitive Data Types
        //predefined within the Java programming language which is used for single-valued variables with limited capabilities

        int wholeNumber = 100;
        System.out.println(wholeNumber);

        //long
        // L is added to the end of the long numbers to be recognized
        long worldPopulation = 9876543211112321L;


        //float
        // add f at the end of a float
        float piFloat = 3.141592653659f;

        System.out.println(piFloat);

        //double - floating point values
        double piDouble = 3.141592653659;
        System.out.println(piDouble);


        //char - single character
        char letter = 'c';
        System.out.println(letter);

        //Boolean
        boolean isLove = true;
        boolean isTaken = false;
        System.out.println(isTaken);
        System.out.println(isLove);

        //constant
        //final - keyword
        //common practice - CAPITAL LETTERS
        final int PRINCIPAL = 3000;
        System.out.println(PRINCIPAL);

        //non-primitive data types
        //can store multiple values
        //aka as reference data types - refer to instances or objects
        //do not directly store the value of a variable, but rather remembers the reference to the variable

        //String
        //stores a sequence or array of characters
        String userName = "Lakwents";
        System.out.println(userName);
        //Sample String methods
        int stringLength = userName.length();
        System.out.println(stringLength);
    }
}
