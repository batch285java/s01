import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("First Name: ");
        String firstName = input.nextLine();
        System.out.println("Last Name: ");
        String lastName = input.nextLine();
        System.out.println("First Subject Grade: ");
        double grade1 = input.nextDouble();
        System.out.println("Second Subject Grade: ");
        double grade2 = input.nextDouble();
        System.out.println("Third Subject Grade: ");
        double grade3 = input.nextDouble();


        int average = (int) ((grade1 + grade2 + grade3) / 3);


        System.out.println("Good Day, " + firstName + " " + lastName+".");
        System.out.println("Your grade average is: " + average);
    }
}
